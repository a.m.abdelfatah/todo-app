import { ActivatedRoute } from '@angular/router';
import { AppConstant } from '../../constants/app.conestant';
import { DatastoreService } from '../../services/datastore.service';
 import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  pageTitle:string;
  constructor(
      private route:ActivatedRoute
  ) {
    this.route.queryParamMap.subscribe((response) => {
      if (response && response['params']) {
        if(response['params'].hasOwnProperty('group')) {
           this.pageTitle='Filter By Group';
        }else if(response['params'].hasOwnProperty('all')){
          this.pageTitle='All Tasks';
        }else if(response['params'].hasOwnProperty('status')){
          this.pageTitle='Filter By Status';

        }
        else if(response['params'].hasOwnProperty('deleted')){
          this.pageTitle='Deleted Tasks';

        }
        else if(response['params'].hasOwnProperty('week')|| response['params'].hasOwnProperty('today')){
          this.pageTitle='Today';

        }
      }
      if(window.location.href.includes('new-task')){
        this.pageTitle = "New Task";
      }
    });

  }
   ngOnInit(): void {
   }


}

import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  Router,
} from '@angular/router';
import { TasksService } from '../../services/tasks.service';
import { Task } from '../../models/group';
import * as moment from 'moment';

@Component({
  selector: 'app-add-task',
  templateUrl: './new-task.component.html',
  styleUrls: ['./new-task.component.scss'],
})
export class NewTaskComponent {
  taskForm: FormGroup;
  groups: any;
  submitted: boolean = false;
  updateMode: boolean = false;
  selectedPriority: string;
  constructor(
    private formBuilder: FormBuilder,
    private tasksService: TasksService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.taskForm = this.formBuilder.group({
      id: [''],
      title: ['', Validators.required],
      description: ['', Validators.required],
      priority: ['', Validators.required],
      deliveryDate: ['', Validators.required],
      group: ['', Validators.required],
      status: [''],
    });

    this.groups = this.tasksService.getAllGroups();
    // Subscribe to the parameter map that includes the `id` parameter.
    this.activatedRoute.paramMap.subscribe((res) => {
       //  Check if there is an `id` in the param map. If so, we are updating a task not creating one.
      if (res && Object.keys(res['params'].length)) {
        this.updateMode = true;

        //  Get the task by its ID and set it on our component.
        let details: any = this.tasksService.getTaskByGroupIdAndTaskId(
          res['params']['gId'],
          res['params']['id']
        );


        if (details) {
          //
          // Set the values of our form to those from the service call.
          const [day, month, year] = details.deliveryDate.split('/');
          // Create a new Date object using the extracted values
          const formattedDate = new Date(
            `${month} ${day} ${year} 00:00:00 GMT+0200`
          );
          let taskDetails = {
            ...details,
            deliveryDate: formattedDate,
            group: Number(res['params']['gId']),
          };
          this.taskForm.setValue(taskDetails);
        }

      }
    });
  }
  //
  // Function called when the form is submitted. Calls either create or update depending on whether we're adding or editing a task.
  addTask() {
    this.submitted = true; //  Set submitted to true when adding a task.

    if (this.taskForm.valid) {
      let inputDate = new Date(this.taskForm.value.deliveryDate);
      const formattedDate = moment(inputDate).format('DD/MM/YYYY');

      const task: Task = {
        id: !this.updateMode ? new Date().valueOf() : this.taskForm.value.id,
        title: this.taskForm.value.title,
        description: this.taskForm.value.description,
        priority: this.taskForm.value.priority,
        deliveryDate: formattedDate,
        status: !this.updateMode ? 'new' : this.taskForm.value.status,
      };
      if (!this.updateMode) { // If we are not updating an existing task...
        this.tasksService.createTask(this.taskForm.value.group, task);
      } else {
        //  Update the task with the changes made in the form.
        this.tasksService.updateTask(this.taskForm.value.group, task);
      }
      this.taskForm.reset();
      this.router.navigate(['/task-list']);
    }
  }

  clearForm() { //  Function that resets the form fields and navigates back to the tasks list page.
    this.taskForm.reset();
    this.taskForm.clearValidators();
    this.taskForm.clearAsyncValidators();
    Object.keys(this.taskForm.controls).forEach((controlName) => {
      this.taskForm.get(controlName).clearValidators();
      this.taskForm.get(controlName).updateValueAndValidity();
    });
  }

  backToList() {
    this.router.navigate(['/task-list']);
  }
}

import { group } from '@angular/animations';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { map, startWith } from 'rxjs';
import { Observable } from 'rxjs';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { Group, Task } from '../../models/group';
import { TasksService } from '../../services/tasks.service';

@Component({
  selector: 'app-group-list',
  templateUrl: './group-list.component.html',
  styleUrls: ['./group-list.component.scss'],
})
export class GroupListComponent implements OnInit {
  @Component({
    selector: 'app-filter-tasks',
    templateUrl: './filter-tasks.component.html',
    styleUrls: ['./filter-tasks.component.css'],
  })
  groups: Group[] = [];
  selectedGroup: any[] = []; // Array to store selected tasks

  filteredGroup: any[] = []; // Initialize to empty array
  taskCtrl = new FormControl();
  filteredDateCtrl = new FormControl();
  separatorKeysCodes = [ENTER];
  filteredGroups: Observable<any[]>;
  searchTextCtrl = new FormControl('');
  toggledTasks: any[]=[];

  constructor(private router:Router,private taskService:TasksService,private route:ActivatedRoute ) {
    this.groups=this.taskService.getAllGroups()
    this.filteredGroups = <any>this.taskCtrl.valueChanges.pipe(
      startWith(null),
      map((group: any | null) =>
        group ? this.filterTasks(group) : this.groups.slice()
      )
    );

    this.filteredDateCtrl.valueChanges.pipe().subscribe((val) => {
       // let date =`${new Date(val).getDate()}/${new  Date().getMonth()+1}/${new Date().getFullYear()}`;
      this.filterGroupsByTaskDate(val);
    });

    this.searchTextCtrl.valueChanges.subscribe((val) => {
      this.filterGroupsByTaskTitle(val);
    });


    this.route.queryParamMap.subscribe(res=>{
      if(res && res['params']){

       this.handelQueryPrams(res);

      }else{
         this.filteredGroup = this.groups
      }
    })
  }
  //  Function to handle query params from url
  handelQueryPrams(response){
    //  If there is a param in the URL then filter by that else show all tasks.
    if(response['params'].hasOwnProperty('group')) {

      this.filterTasks(response['params']['group']); //  Filtering Tasks By Group Name
    }else if(response['params'].hasOwnProperty('all')){
      this.filteredGroup = this.groups //  Show All Tasks
    }else if(response['params'].hasOwnProperty('status')){
      this.filterGroupsByTaskStatus(response['params']['status']) //   Filtering TasksBy Status
    }
    else if(response['params'].hasOwnProperty('deleted')){
      this.getDeletedTasks(); //  Getting Deleted Tasks
    }
    else if(response['params'].hasOwnProperty('week')|| response['params'].hasOwnProperty('today')){
      const currentDate = new Date();
      this.filterGroupsByTaskDate(currentDate); //   Filtering Tasks for Today or This Week
    }
  }

  ngOnInit() {
    this.filteredGroup = this.groups; // Initially show all groups
  }

  filterGroupsByTaskTitle(searchText) { //  Searches Groups based on search text provided in input field.
    return (this.filteredGroup = this.groups.filter((group) => {
      return group.tasks.some((task) =>
        task.title.toLowerCase().includes(searchText.toLowerCase())
      );
    }));
  }


  filterGroupsByTaskStatus(status) { //  Filters Tasks Based On The Provided Status In Url Params
    return (this.filteredGroup = this.groups.filter((group) => {
      return group.tasks.some((task) =>
        task.status.toLowerCase().includes(status.toLowerCase())
      );
    }));
  }


  getDeletedTasks() { //  Gets the deleted tasks from localStorage and assign it to 'deletedTasks' array
    return (this.filteredGroup = this.taskService.getDeletedTasks() )
  }



  //filter tasks by task date
  filterGroupsByTaskDate(searchDate: any) {
    if (!searchDate) {
      return this.filteredGroup = this.groups; // Handle empty search date (optional)
    }

    const searchTimestamp = moment(searchDate).startOf('day').valueOf(); // Get timestamp without time

    this.filteredGroup = this.groups.filter((group) => {
      return group.tasks.some((task) => {
        const taskTimestamp = moment(task.deliveryDate, 'DD/MM/YYYY').startOf('day').valueOf(); // Get timestamp without time
        return taskTimestamp === searchTimestamp;
      });
    });
  }

  //filter tasks by Group
  filterTasks(value: any) {
    let filterValue;
    if (value && typeof value != 'string') {
      filterValue = value['name'].toLowerCase();
    } else {
      filterValue = value.toLowerCase();
    }

    this.filteredGroup = this.groups.filter(
      (task) => task.name.toLowerCase().indexOf(filterValue) !== -1
    );
  }

  add(event: any): void { //  Adds a new Group in the selected Group with given values
    const input = event.chipInput;
    const value = event.value;

    if ((value || '').trim()) {
      this.taskCtrl.setValue(null);
      this.filteredGroup.push(value); // Add to filteredGroup for display
    }

    if (input) {
      input.value = '';
    }
  }

  remove(task: any): void { //  Removes an existing Group from the list of Groups.
    const index = this.filteredGroup.indexOf(task);

    if (index >= 0) {
      this.filteredGroup.splice(index, 1);
    }
    if (this.filteredGroup.length == 0) this.filteredGroup = this.groups;
  }

  // Implement selected method if needed based on your use case
  selected(event: MatAutocompleteSelectedEvent): void {
    // Handle user selection from autocomplete
    const selectedTask = event.option.value;
    if (!this.selectedGroup.includes(selectedTask)) {
      this.selectedGroup.push(selectedTask);
    }
    this.filteredGroup = this.selectedGroup;
  }




  newTask(){
    this.router.navigate(['new-task'])
  }


  deleteTask(event){ //  Deletes a task by its ID.
    this.taskService.deleteTask(event.groupId,event.taskId)
    this.groups = this.taskService.getAllGroups()
    this.filteredGroup = this.groups
   }

   updateStatus(event){ //  Updates status of a Task to completed or not_completed.

    this.taskService.updateStatus(event.groupId,event.taskId,event.status);
    this.groups = this.taskService.getAllGroups()
    this.filteredGroup = this.groups
   }

   toggleTask(task) { //  Toggles the done property of a given task.
    if (task) {
       const taskIndex = this.toggledTasks.findIndex((val) => val?.task.id === task.task.id);
      if (taskIndex === -1) {
        this.toggledTasks.push(task);
      } else {
        this.toggledTasks.splice(taskIndex, 1);
      }
    }

  }

  deleteMulti(){ //  Delete multiple tasks at once.
    this.toggledTasks.forEach(element => {
      this.taskService.deleteTask(element.groupId, element.task.id)
      this.groups = this.taskService.getAllGroups()
      this.filteredGroup = this.groups
    });
  }

  updateMultiStatus(){ //  Update the status of multiple tasks at once.
    this.toggledTasks.forEach(element => {
      this.taskService.updateStatus(element.groupId, element.task.id,'DONE')
      this.groups = this.taskService.getAllGroups()
      this.filteredGroup = this.groups
    });
  }

  filterByGroup(group){
  return  this.groups.filter(x=> x.name == group);
  }
}

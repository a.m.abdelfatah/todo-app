export interface Task {
  id: number;
  title: string;
  deliveryDate: string;
  status: string;
  description:string;
  priority:string;

}

export interface Group {
  id: number;
  name: string;
  tasks: Task[];
}

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Task } from '../../models/group';

@Component({
  selector: 'app-task-item',
  templateUrl: './task-item.component.html',
  styleUrls: ['./task-item.component.scss']
})
export class TaskItemComponent implements OnInit {
@Input() taskDetails:any={}
@Input() groupId:any;
@Output()  deleteTaskEmitter = new EventEmitter();
@Output() updateStatusEmitter=new EventEmitter();
@Output() toggledTasksEmitter=new EventEmitter();
toggledTasks:Task[]=[];
  constructor() { }

  ngOnInit() {
  }
  deleteTask(id){
    this.deleteTaskEmitter.emit({groupId:this.groupId,taskId:id})
  }
  onChangeStatus(status,taskId){
    this.updateStatusEmitter.emit({status:status,groupId:this.groupId,taskId:taskId})
  }

  toggleTask(task){
    this.toggledTasksEmitter.emit({groupId:this.groupId,task:task})
  }
}

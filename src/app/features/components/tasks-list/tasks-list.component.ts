import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-tasks-list',
  templateUrl: './tasks-list.component.html',
  styleUrls: ['./tasks-list.component.css'],
  changeDetection:ChangeDetectionStrategy.OnPush
})
export class TasksListComponent implements OnInit{
@Input() taskList:any[]=[];
@Input() groupId:any;
@Output()  deleteTaskEmitter = new EventEmitter();
@Output() updateStatusEmitter=new EventEmitter();
@Output() toggledTasksEmitter=new EventEmitter();

  constructor() {

   }

  ngOnInit() {
  }
  deleteTask(event){
    this.deleteTaskEmitter.emit(event)
  }
  updateStatus(event){
    this.updateStatusEmitter.emit(event)
  }

  toggleTask(task){
    this.toggledTasksEmitter.emit(task)
  }
}

import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DatastoreService } from '../../shared/services/datastore.service';
import { Group, Task } from '../models/group';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class TasksService {
  private tasksSubject: BehaviorSubject<Group[]>;
  public tasks: Observable<Group[]>;

  constructor(private dataStoreService: DatastoreService) {
    this.tasksSubject = new BehaviorSubject<Group[]>(this.getAllGroups());
    this.tasks = this.tasksSubject.asObservable();
  }

  getAllGroups(): Group[] {
    return this.dataStoreService.getItemFromLocalStorage('groups', true);
  }

  getGroupById(id: number): Group {
    return this.getAllGroups().find((group) => group.id == id);
  }

  getTaskByGroupIdAndTaskId(groupId: number, taskId: number): Task {
    const group = this.getGroupById(groupId);
    return group ? group.tasks.find((task) => task.id == taskId) : null;
  }

  //  Add a new task to a group and save it in local storage
  createTask(groupId: number, task: Task): void {
    const groups: Group[] = this.getAllGroups();
    const group = this.getGroupById(groupId);
    const groupIndex = groups.findIndex(group => group.id === groupId);

    if (group) {
      task.id =
        group.tasks.length > 0
          ? Math.max(...group.tasks.map((t) => t.id)) + 1
          : 1;
      groups[groupIndex].tasks.push(task);

      this.dataStoreService.setInLocalStorage('groups', groups, true);
      this.tasksSubject.next(groups);
    }
  }

  // update task details
  updateTask(groupId: number, task: Task): void {
    const groups: Group[] = this.getAllGroups();
    const group = this.getGroupById(groupId);
    const groupIndex = groups.findIndex(group => group.id === groupId);

    if (group) {
      const index = group.tasks.findIndex((t) => t.id === task.id);
      groups[groupIndex].tasks[index] = task;
      this.dataStoreService.setInLocalStorage('groups', groups, true);
      this.tasksSubject.next(groups);
    }
  }

 // update Status of a specific task in the tasks array and save it to local storage
  updateStatus(groupId: number, taskId, status): void {
    const groups: Group[] = this.getAllGroups();
    const group = this.getGroupById(groupId);
    const groupIndex = groups.findIndex(group => group.id === groupId);

    if (group) {
      const index = group.tasks.findIndex((t) => t.id == taskId);
      const task = groups[groupIndex].tasks[index]
      groups[groupIndex].tasks[index] = { ...task, status: status };
      this.dataStoreService.setInLocalStorage('groups', groups, true);
      this.tasksSubject.next(groups);
    }
  }

  //  delete a task from the list of tasks in the store
  deleteTask(groupId: number, taskId: number): void {
    const groups: Group[] = this.getAllGroups(); //  get the current data from local storage
    const group = this.getGroupById(groupId); //  find the group by its id

    if (group) {
      const updatedGroups = groups.map(g => ({
        ...g,
        tasks: g.id === groupId ? g.tasks.filter(task => task.id !== taskId) : g.tasks
      }));

      this.dataStoreService.setInLocalStorage('groups', updatedGroups, true); //  save the new data to local storage

      //  get the deleted task from the localStorage
      const deletedGroupsArray: Group[] = this.dataStoreService.getItemFromLocalStorage('deletedGroups', true) || [];
    //  check if there is a deleted group with that Id
      const existingDeletedGroup = deletedGroupsArray.find(g => g.id === groupId);
      //   add the group to the array of deleted groups and save it in local Storage
      if (!existingDeletedGroup) {
        deletedGroupsArray.push({ ...group, tasks: [group.tasks.find(task => task.id === taskId)] });
      } else {
        // if group has removed tasks  then just remove it from the GroupsArray and
        // saved it directly to local storage under it's won group
        existingDeletedGroup.tasks.push(...group.tasks.filter(task => task.id === taskId));
      }

      this.dataStoreService.setInLocalStorage('deletedGroups', deletedGroupsArray, true);
      this.tasksSubject.next(updatedGroups);
    }
  }
 //
  initData(): void {
    if (!this.getAllGroups()) {
      const initialData = [
        {
          id: 1,
          name: 'Group 1',
          tasks: [
            {
              id: 1,
              title: 'Coffee bears',
              deliveryDate: '22/02/2024',
              status: 'done',
              description: 'pla pla',
              priority: 'Low',
            },
            {
              id: 2,
              title: 'Coffee bears',
              deliveryDate: '21/02/2024',
              status: 'progress',
              description: 'pla pla',
              priority: 'Low',
            },
            {
              id: 3,
              title: 'Coffee bears',
              deliveryDate: '20/02/2024',
              status: 'done',
              description: 'pla pla',
              priority: 'Low',
            },
          ],
        },
        {
          id: 2,
          name: 'Group 2',
          tasks: [
            {
              id: 4,
              title: 'Mortgage Repayment',
              deliveryDate: '19/02/2024',
              status: 'done',
              description: 'pla pla',
              priority: 'Low',
            },
            {
              id: 5,
              title: 'Mortgage Repayment',
              deliveryDate: '18/02/2024',
              status: 'progress',
              description: 'pla pla',
              priority: 'Low',
            },
            {
              id: 6,
              title: 'Mortgage Repayment',
              deliveryDate: '13/02/2024',
              status: 'done',
              description: 'pla pla',
              priority: 'Low',
            },
          ],
        },
        {
          id: 3,
          name: 'Group 3',
          tasks: [
            {
              id: 7,
              title: 'Milk',
              deliveryDate: '21/02/2024',
              status: 'done',
              description: 'pla pla',
              priority: 'Low',
            },
            {
              id: 8,
              title: 'Milk',
              deliveryDate: '21/02/2024',
              status: 'progress',
              description: 'pla pla',
              priority: 'Low',
            },
            {
              id: 9,
              title: 'Milk',
              deliveryDate: '21/02/2024',
              status: 'done',
              description: 'pla pla',
              priority: 'Low',
            },
          ],
        },
      ];

      this.dataStoreService.setInLocalStorage('groups', initialData, true);
      this.tasksSubject.next(initialData);
    }
  }

  getDeletedTasks(){
    return this.dataStoreService.getItemFromLocalStorage('deletedGroups', true)||[]
  }
}

import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
 import { SharedModule } from './shared/shared.module';
 import { HttpClientModule } from '@angular/common/http';
 import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SidebarComponent } from './shared/components/sidebar/sidebar.component';
import { TaskItemComponent } from './features/components/task-item/task-item.component';
import { TasksListComponent } from './features/components/tasks-list/tasks-list.component';
import { GroupListComponent } from './features/pages/group-list/group-list.component';
import { NewTaskComponent } from './features/pages/new-task/new-task.component';


@NgModule({
  declarations: [
    AppComponent,
     SidebarComponent,
     TaskItemComponent,
     TasksListComponent,
     GroupListComponent,
     NewTaskComponent
   ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule { }

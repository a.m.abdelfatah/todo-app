import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TasksListComponent } from './features/components/tasks-list/tasks-list.component';
import { GroupListComponent } from './features/pages/group-list/group-list.component';
import { NewTaskComponent } from './features/pages/new-task/new-task.component';

const routes: Routes = [
{path:'task-list',component:GroupListComponent},
{path:'new-task',component:NewTaskComponent},
{path:'task-details/:id/:gId',component:NewTaskComponent},
  {path:'', redirectTo:'task-list', pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

 import { Component } from '@angular/core';
import { TasksService } from './features/services/tasks.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'todo-list';
  isLoggedIn:boolean=false
  constructor(private taskService:TasksService ){
    this.taskService.initData()
  }
}
